import threading
import tempfile
import queue
import sys
from datetime import datetime

import sounddevice as sd
import soundfile as sf

import numpy
assert numpy


def record(stop_event, filename=None):
    """ Record an audio and save it to file"""
    device = sd.default.device
    device_info = sd.query_devices(device, 'input')
    q = queue.Queue()

    def callback(indata, frames, time, status):
        if status:
            print(status, file=sys.stderr)
        q.put(indata.copy())

    try:
        channels = 2
        samplerate = int(device_info['default_samplerate'])
        if filename is None:
            filename = tempfile.mktemp(
                    prefix=datetime.now().strftime('%y%m%d_%H%M%S_'),
                    suffix='.wav',
                    dir='recs')

        with sf.SoundFile(
                filename, mode='x', samplerate=samplerate, channels=channels, subtype="PCM_24"
                ) as file:

            with sd.InputStream(
                    samplerate=samplerate, device=device, channels=channels, callback=callback
                    ):

                while not stop_event.is_set():
                    file.write(q.get())
                print(f'Saved recording in file {filename}')
    except KeyboardInterrupt:
        print('Interrupted by keyboard')


def playback(filename, on_end=None):
    device = sd.default.device
    event = threading.Event()
    try:
        data, fs = sf.read(filename, always_2d=True)
        current_frame = 0

        def callback(outdata, frames, time, status):
            nonlocal current_frame
            if status:
                print(status)
            chunksize = min(len(data) - current_frame, frames)
            outdata[:chunksize] = data[current_frame:current_frame + chunksize]
            if chunksize < frames:
                outdata[chunksize:] = 0
                raise sd.CallbackStop()
            current_frame += chunksize

        stream = sd.OutputStream(
                samplerate=fs, device=device, channels=data.shape[1],
                callback=callback, finished_callback=event.set)
        with stream:
            event.wait()  # Wait until playback is finished
            if callable(on_end):
                on_end()
    except Exception as e:
        print(e)
