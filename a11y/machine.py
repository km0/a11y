from statemachine import StateMachine, State
import threading
from . import audio
import os
from glob import glob
import random


class A11yMachine(StateMachine):
    "State machine for the A11y device"

    print('\nWelcome to a11y!')
    os.makedirs('recs', exist_ok=True)
    recording_list = random.sample(glob('recs/*.wav'), len(glob('recs/*.wav')))

    recording_thread = None
    info_file = "recs/info.wav"
    current_playing = ""
    playback_timeout = None

    # States
    idle = State(initial=True)
    recording = State()
    playback = State()
    info = State()
    recording_info = State()

    # Events
    push = (
            idle.to(playback)
            | playback.to(playback)
        )
    hold = (
            idle.to(recording)
            | playback.to(recording)
            | info.to(recording_info)
        )
    release = (
            recording.to(idle)
            | recording_info.to(idle)
        )
    double_push = (
            idle.to(info)
            | playback.to(info)
        )
    end = (
            playback.to(idle)
            | info.to(idle)
        )

    # Stop playback
    @recording.enter
    @playback.enter
    @info.enter
    @recording_info.enter
    def stop_playback(self):
        if (self.playback_timeout):
            self.playback_timeout.cancel()

    @recording.enter
    def start_recording(self):
        print("RECORDING: Recording in progress...")
        self.recording_event = threading.Event()
        threading.Thread(
                target=audio.record, args=(self.recording_event,)
                ).start()

    def on_release(self):
        if self.recording_event is not None:
            self.recording_event.set()

    @playback.enter
    def start_playback(self, filename=None):
        if filename is None:
            if len(self.recording_list) == 0:
                self.recording_list = self.list_files()
            if len(self.recording_list) > 0:
                filename = self.recording_list.pop()

        print(f"PLAYBACK: Reproducing {filename}")
        audio.playback(filename, self.finish_playback)

    def finish_playback(self):
        print("Playback done!")
        self.send("end")

    @info.enter
    def get_info(self):
        print(f"INFO: Reproducting {self.info_file}")
        audio.playback(self.info_file, self.finish_playback)

    @recording_info.enter
    def set_info(self, filename='recs/info.wav'):
        print("RECORDING INFO: Recording in progress...")
        self.info_file = filename
        self.recording_event = threading.Event()
        threading.Thread(
                target=audio.record, args=(self.recording_event, filename)
                ).start()

    def list_files(self):
        return random.sample(glob('recs/*.wav'), len(glob('recs/*.wav')))
