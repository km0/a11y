from . import machine
from gpiozero import Button
from signal import pause

import warnings
warnings.simplefilter('ignore')

sm = machine.A11yMachine()
Button.was_held = False


def hold(btn):
    global sm
    btn.was_held = True
    sm.send('hold')


def release(btn):
    global sm
    if not btn.was_held:
        press()
    else:
        sm.send('release')
    btn.was_held = False


def press():
    global sm
    sm.send('push')


def start():
    button = Button(2, hold_time=0.5)

    button.when_held = hold
    button.when_released = release
    pause()
