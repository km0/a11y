from a11y import machine
from time import sleep


def test():
    sm = machine.A11yMachine()
    # img_path = "./diagram.png"
    # sm._graph().write_png(img_path)

    # Idle state
    # Push to play audio and wait for it to finish
    sm.send('push', 'ding_dong.wav')
    # sleep(3)

    # Hold to record audio
    sm.send('hold')
    sleep(1)
    print('One second passed...')
    sleep(1)
    print('Two seconds passed...')

    # Release after two seconds
    sm.send('release')

    # Double push to play info
    sm.send('double_push')
    sleep(0.5)

    # Hold to record info file
    sm.send('hold')
    sleep(2)

    sm.send('release')

    # Double push to play info
    sm.send('double_push')
